/* eslint no-unused-vars: 1, func-style: 1 */
// Silence
function onloadTZSetup() {}

// Search form input handling
const input = document.querySelector('form input[type=text]')

if (input) {
  input.setAttribute('placeholder', 'Search')

  // Toggle input text
  input.addEventListener('click', (e) => {
    e.stopPropagation()

    if (input.value === input.defaultValue) {
      input.value = ''
    }
  })

  document.addEventListener('click', () => {
    if (input.value === '') {
      input.value = input.defaultValue
    }
  })
}
