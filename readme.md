## about

A minimalistic [`gitweb(1)`](https://git-scm.com/docs/gitweb) theme to prettify self hosted web facing repository listings.

## setup

Download somewhere convenient keeping in mind, by default, paths are relative to `/usr/share/gitweb`. For example,

```sh
# Fetch latest
git clone https://github.com/thewhodidthis/webgit ~/

# Symlink into place
sudo ln -s ~/webgit /usr/share/gitweb
```

## usage

Sample [`gitweb.conf(5)`](https://git-scm.com/docs/gitweb.conf) edits:

```perl
# /etc/gitweb.conf

$feature{'remote_heads'}{'default'} = [1];
$feature{'search'}{'default'} = [];
$feature{'avatar'}{'default'} = ['gravatar'];
$feature{'javascript-timezone'}{'default'} = [];

$javascript = "webgit/index.js";

$logo = "webgit/static/git-logo.png";
$favicon = "webgit/static/git-favicon.png";
$site_html_head_string = "<meta name=\"viewport\" content=\"width=device-width\" />";
$omit_owner = true;

@stylesheets = ("webgit/index.css");
@diff_opts = ();
```

To generate a web accessible `readme.html`, assuming [Markdown.pl](https://daringfireball.net/projects/markdown/) installed, run the following from within the project folder:

```sh
git cat-file blob HEAD:readme.md | markdown > .git/readme.html
```

## see also

- [List of Git front ends](https://git.wiki.kernel.org/index.php/Interfaces,_frontends,_and_tools)
- [Stagit](https://git.codemadness.org/stagit/)
